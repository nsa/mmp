#!/usr/bin/env php
<?php

if( !defined( __DIR__ ) )define( __DIR__, dirname(__FILE__) );

require_once __DIR__.'/init.php';

$cli_params = Helper::parseCommandLineArgs($argv);

foreach ($cli_params['command']['args'] as $key => $value) {
 	if($value == '--config'){
 		$cli_params['options']['config'] = $cli_params['command']['args'][$key+1];
		$cli_params['command']['args'][$key+1]=null;
		$cli_params['command']['args'][$key]=null;
 	}
}

if(empty($cli_params['options']['config']))
{
  $cli_params['options']['config'] = __DIR__ . DIR_SEP . 'config.ini';	
}

if(file_exists($cli_params['options']['config']))
{
  $config = parse_ini_file($cli_params['options']['config']);
  $config = array_replace($config, $cli_params['options']); //command line overrides everything
}
else
{
  Output::error('mmp: could not find config file "' . $cli_params['options']['config'] . '"');
  exit(1);
}

Helper::setConfig($config);

$controller = Helper::getController($cli_params['command']['name'], $cli_params['command']['args']);
if($controller !== false)
{
  $controller->runStrategy();
}
else
{
  Output::error('mmp: unknown command "' . $cli_params['command']['name'] . '"');
  Helper::getController('help')->runStrategy();
  exit(1);
}

