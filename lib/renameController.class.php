<?php

class renameController extends AbstractController
{
  
  protected $queries = array();
  
  public function runStrategy()
  {

    $db = Helper::getDbObject();
		
	if(empty($this->args) || count($this->args) != 3){
      echo 'Please put right arguments' . PHP_EOL;
      exit(0);
	}
	
	$difference = array();
	
	$table = $this->args[0];
	$col1 = $this->args[1];
	$col2 = $this->args[2];
	
	$res = $db->query("SHOW FIELDS FROM `{$table}` WHERE `Field`='{$col1}' OR `Field`='{$col2}';");
	
	if(!$res){
	  echo 'Columns or table not found' . PHP_EOL;
      exit(0);
	}
	
	$fields = array();
    while ($row = $res->fetch_array(MYSQLI_NUM))
    {
      $fields[$row[0]] = $row;
    }
		
	if(!isset($fields[$col1])){
	  echo 'Column not found' . PHP_EOL;
      exit(0);
	}
	
	if(isset($fields[$col2]))
	{
      echo 'Second column already existing' . PHP_EOL;
      exit(0);
    }
	
	$difference['up'][] = "ALTER TABLE `".$table."` CHANGE `".$col1."` `".$col2."` ".$fields[$col1][1].";";
	$difference['down'][] = "ALTER TABLE `".$table."` CHANGE `".$col2."` `".$col1."` ".$fields[$col1][1].";";
    
    if (!count($difference['up']) && !count($difference['down']))
    {
      echo 'Your database has no changes from last revision' . PHP_EOL;
      exit(0);
    }
	
	$db->query($difference['up'][0]);
	Output::verbose("UP: ".$difference['up'][0]);

    $version = Helper::getCurrentVersion();
    $filename = Helper::get('savedir') . "/migration{$version}.php";
    $content = Helper::createMigrationContent($version, $difference);
    file_put_contents($filename, $content);
    Output::verbose("file: {$filename} written!");
    $vTab = Helper::get('versiontable');
    $db->query("INSERT INTO `{$vTab}` SET rev={$version}");
  }

}
