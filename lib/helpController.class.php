<?php

class helpController extends AbstractController
{

  public function runStrategy()
  {
    $output = <<<HELP

MySQL Migration with PHP

Usage:
  ./migrate.php [options] command [command arguments]

Available commands:

  \033[1;32mhelp:\033[0m       display this help and exit
  \033[1;32mschema:\033[0m     create schema for initial migration/installation
  \033[1;32minit:\033[0m       load initial schema (install)
  \033[1;32mcreate:\033[0m     create new migration
  \033[1;32mrename:\033[0m     rename table column
  \033[1;32mlist:\033[0m       list available migrations and mark current version
  \033[1;32mmigrate:\033[0m    migrate to specified time
  
Shortcuts from root dir:
  
  Windows: ./db.bat
  UNIX: ./db

Available options:

  --config    Path to alternate config.ini file that will override the default
 
For migrate command you can use strtotime format
Examples:
*********************************************************************
./db rename table column1 column2
./db migrate yesterday
./db migrate -2 hour
./db migrate +2 month
./db migrate 20 September 2001
./db migrate
********************************************************************
Last example will update your database to latest version

HELP;

    //Strip color output since Windows doesn't support it
    if (PHP_OS === 'WINNT')
    {
      $output = preg_replace('/\\033\[\d+(;\d+)?m/i', '', $output);
    }

    echo $output;
  }
}
